<?php
class Netstarter_Content_PageController extends Mage_Core_Controller_Front_Action{
    
    public function viewAction(){
        $page = Mage::getModel("netstarter_content/page")->getPage($this->getRequest()->getParam('nodealiaspath'));
        
        if($page){
            $this->loadLayout();
            //Add content
            $this->getLayout()->getBlock('content_page_view')->setData('html_content', $page->getContent());
            //Add title
            if($page->getMetaTitle()){
                $this->getLayout()->getBlock('head')->setTitle($page->getMetaTitle());
            }
            //Add description
            if($page->getMetaDescription()){
                $this->getLayout()->getBlock('head')->setDescription($page->getMetaDescription());
            }
            //Add keyword
            if($page->getMetaKeyword()){
                $this->getLayout()->getBlock('head')->setKeywords($page->getMetaKeyword());
            }

            $this->renderLayout();
        
        }else{
            //If there is no such page in Kentico then display 404 page.
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultNoRoute');
            }
        }
        
    }
    public function clearAction(){
        $cache = Mage::app()->getCache();
        $nodealiaspath = $this->getRequest()->getParam('nodealiaspath');
        $cacheKey = str_replace("/","-",$nodealiaspath);
        $cache->remove($cacheKey);
        echo "Cache cleared";
    }
    public function pageAction(){
        $banner = Mage::getModel('netstarter_content/banner')->getCategoryBanner("/category/body");
        //echo "Kentico Page:" . $this->getRequest()->getParam('nodealiaspath');
        /*$url = "http://messagenet-cms.webuat.sd-svc.net/rest/content/site/corporatesite/en-us/all/news?format=json&classnames=cms.news&columns=NodeAliasPath,NewsTitle,NewsSummary,NewsText";
        $newslist = $this->getResult($url);
        $newslist = $newslist["cms_news"];
        foreach ($newslist as $news) {
            echo "<a href=\"/content" . strtolower($news["NodeAliasPath"]) . "\">" . $news["NewsTitle"] . "</a><br/>";
            
        }
        
        $url = "http://messagenet-cms.webuat.sd-svc.net/rest/content/site/corporatesite/en-us/document/news/apple-ipad-2-in-stock?format=json&classnames=cms.news&columns=NewsTitle,NewsSummary,NewsText";
        $news = $this->getRestAPIResult($url);
        $news = $news["cms_news"];
        $news = $news[0];
        echo $news["NewsText"];*/
    }
    private function getRestAPIResult($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('content-type: text/xml','Accept: version_2.0'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERPWD, "administrator:pass123@");
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        $kenticoObject = json_decode($output, true);
        $kenticoObject = $kenticoObject["cms_documents"][0];
        return $kenticoObject;
    }
    private function getPageResult($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('content-type: text/xml','Accept: version_2.0'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        if($info["http_code"] == 200)
            return $output;
        else
            return "";
    }
}