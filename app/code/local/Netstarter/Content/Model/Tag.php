<?php
    
class Netstarter_Content_Model_Tag extends Mage_Core_Model_Abstract{
    private $kenticoUrl;
    private $kenticoUserName;
    private $kenticoPassword;
    private $kenticoSite;
    public function _construct(){
        $configData = Mage::getStoreConfig('content/config');
        $this->kenticoUrl = $configData["kenticourl"];
        $this->kenticoUserName = $configData["kenticousername"];
        $this->kenticoPassword = $configData["kenticopassword"];
        $this->kenticoSite = $configData["kenticosite"];
        parent::_construct();
    }
    
    public function getTagPages($contentTag){
        $tags = explode(",", $contentTag);
        $tagWhere = "";
        foreach ($tags as $t){
            $tagWhere .= ($tagWhere?"+or+":"") . "DocumentTags+like+%27%" . trim($t) . "%25%27";
        }
        $url = "/all?where=" . $tagWhere . "&classnames=cms.blogpost&columns=DocumentName,NodeAliasPath,BlogPostSummary,BlogPostTeaser";
        $kenticoPages = $this->getRestAPIResult($url);
        $blogPosts = array();
        foreach($kenticoPages as $k){
            foreach($k as $pages){
                foreach($pages as $page){
                    $blog = new Varien_Object();
                    $blog->setData("blog_post_summary", $page["BlogPostSummary"]);
                    if($page["BlogPostTeaser"])
                        $blog->setData("blog_post_teaser", $this->kenticoUrl . "getattachment/" . $page["BlogPostTeaser"] . "/.aspx?maxsidesize=90");
                    $blog->setData("document_name", $page["DocumentName"]);
                    $blog->setData("node_alias_path", strtolower($page["NodeAliasPath"]));
                    $blogPosts[] = $blog;
                }
            }
        }
        
        ///getattachment/23900c40-1884-4d34-95b9-9b3a5d346b75/.aspx?maxsidesize=90
        return $blogPosts;
        
    }
    
    private function getRestAPIResult($url){
        $url = $this->kenticoUrl . "rest/content/site/" . $this->kenticoSite . "/en-us" . $url . "&format=json";
        //echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('content-type: text/xml','Accept: version_2.0'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERPWD, $this->kenticoUserName . ":" . $this->kenticoPassword);
        //curl_setopt($ch, CURLOPT_USERPWD, "administrator:pass123@");
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        $kenticoObject = json_decode($output, true);
        $kenticoObject = $kenticoObject["cms_documents"];
        return $kenticoObject;
    }
}