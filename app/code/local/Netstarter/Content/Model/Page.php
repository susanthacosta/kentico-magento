<?php

class Netstarter_Content_Model_Page extends Varien_Object
{
    const CACHE_TAG = 'kentico_pge';
    private $kenticoUrl;
    private $kenticoContainer;
    private $kenticoEndContainer;
    public function _construct(){
        $configData = Mage::getStoreConfig('content/config');
        $this->kenticoUrl = $configData["kenticourl"];
        $this->kenticoContainer = $configData["container"];
        $this->kenticoEndContainer = $configData["endcontainer"];
        parent::_construct();
    }
    public function getPage($nodealiaspath){
        $cache = Mage::app()->getCache();
        $cacheKey = str_replace("/","-",$nodealiaspath);
        
        $page = null;//unserialize($cache->load($cacheKey));
        if(!$page){
            $contentPage = $this->getPageResult($this->kenticoUrl . $nodealiaspath);
            if($contentPage){
                preg_match("/(<div class=\"" . $this->kenticoContainer . "\">[\w\W]+)<div class=\"" . $this->kenticoEndContainer . "\">/i", $contentPage, $matches);
                if(isset($matches[1])){
                    $content = $matches[1];
                    if($content){

                        $content = str_replace("/getattachment", $this->kenticoUrl . "getattachment", $content);
                        $content = str_replace("/getmetafile", $this->kenticoUrl . "getmetafile", $content);
                        $content = str_replace("/App_Themes", $this->kenticoUrl . "App_Themes", $content);

                        preg_match_all("/{%([^}]+)%}/i", $content, $productSkus);
                        if($productSkus && sizeof($productSkus)>1){
                            $_productCollection = Mage::getModel('catalog/product')
                                                ->getCollection()
                                                ->addAttributeToSelect('*')
                                                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                                                ->addAttributeToFilter('sku', array('in'=> $productSkus[1]));
                            foreach($_productCollection as $p){
                                $pHtml = "";
                                $pHtml .= "<div class='blog-product-title'><a href='" . $p->getProductUrl() . "'>" . $p->getName() . "</a></div>";
                                $pHtml .= "<div class='blog-product-img'><a href='" . $p->getProductUrl() . "'><img src='" . Mage::helper('catalog/image')->init($p, 'small_image')->resize(240, 240) . "'/></a></div>";
                                $pHtml .= "<div class='blog-product-short-desc'>" . $p->getShortDescription() . "</div>";
                                $content = str_replace("{%" . $p->getSku() . "%}", $pHtml, $content);
                            }
                        }
                        
                        $page = new Varien_Object();
                        $page->setData("content",$content);
                        
                        //Add title
                        preg_match("/<title>([\w\W]+)<\/title>/i", $contentPage, $matches);
                        if(isset($matches[1])){
                            $page->setData("meta_title",trim($matches[1]));
                        }
                        //Add description
                        preg_match("/<meta name=\"description\" content=\"([^\/>]+)\" \/>/i", $contentPage, $matches);
                        if(isset($matches[1])){
                            $page->setData("meta_description",trim($matches[1]));
                        }
                        //Add keyword
                        preg_match("/<meta name=\"keywords\" content=\"([^\/>]+)\" \/>/i", $contentPage, $matches);
                        if(isset($matches[1])){
                            $page->setData("meta_keyword",trim($matches[1]));
                        }
                        $cache->save(serialize($page), $cacheKey, array(Netstarter_Content_Model_Page::CACHE_TAG), 60*60);
                    }
                }
            }
        }
        
        return $page;
    }
    
    private function getPageResult($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('content-type: text/xml','Accept: version_2.0'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        if($info["http_code"] == 200)
            return $output;
        else
            return "";
    }
}
