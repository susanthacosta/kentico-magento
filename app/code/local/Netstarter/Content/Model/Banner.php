<?php
    
class Netstarter_Content_Model_Banner extends Mage_Core_Model_Abstract{
    private $kenticoUrl;
    private $kenticoUserName;
    private $kenticoPassword;
    private $kenticoSite;
    public function _construct(){
        $configData = Mage::getStoreConfig('content/config');
        $this->kenticoUrl = $configData["kenticourl"];
        $this->kenticoUserName = $configData["kenticousername"];
        $this->kenticoPassword = $configData["kenticopassword"];
        $this->kenticoSite = $configData["kenticosite"];
        parent::_construct();
    }
    
    public function getCategoryBanner($bannerPath){
        $url = "/all" . $bannerPath . "?where=%27" . date('Y-m-d') . "%27+between+DocumentPublishFrom+and+DocumentPublishTo&classnames=pagecontent.categorybanner&columns=Title,Description,Image";
        //$url = str_replace(":", "%3A", $url);
        //$url = "/all" . $bannerPath . "?where=%27" . date('Y-m-d H:i:s') . "%27+between+DocumentPublishFrom+and+DocumentPublishTo&classnames=pagecontent.categorybanner&columns=Title,Description,Image";
        $banner = $this->getRestAPIResult($url);
        $banner = $banner["pagecontent_categorybanner"];
        $bannerObject = new Varien_Object();
        if($banner && isset($banner[0])){
            $bannerObject->setData("title", $banner[0]["Title"]);
            $bannerObject->setData("description", $banner[0]["Description"]);
            $bannerObject->setData("image", str_replace("~/", $this->kenticoUrl, $banner[0]["Image"]));
            return $bannerObject;
        }
        return null;
    }
    
    private function getRestAPIResult($url){
        $url = $this->kenticoUrl . "rest/content/site/" . $this->kenticoSite . "/en-us" . $url . "&format=json";
        //echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('content-type: text/xml','Accept: version_2.0'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERPWD, $this->kenticoUserName . ":" . $this->kenticoPassword);
        //curl_setopt($ch, CURLOPT_USERPWD, "administrator:pass123@");
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        //Mage::log($info);
        $kenticoObject = json_decode($output, true);
        if($kenticoObject["cms_documents"] && isset($kenticoObject["cms_documents"][0])){
            return $kenticoObject["cms_documents"][0];
        }else
            null;
    }
}