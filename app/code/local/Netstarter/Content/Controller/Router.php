<?php

class Netstarter_Content_Controller_Router extends Mage_Core_Controller_Varien_Router_Standard
{
    const FRONTEND_SUFFIX = "content";
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();
        //adding our custom router in the loop and we make our own match and nice URL SEO
        $front->addRouter('content', $this);
    }
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            //as cms rounter will do install here just do nothing here
            return false;
        }
        $path = trim($request->getPathInfo(),'/');
        
        //Last router is Kentico content
        $request->setModuleName('content')
                ->setControllerName('page')
                ->setActionName('view')
                ->setParam('nodealiaspath', $path);
        $request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,$path);
        return true;
    }
}
