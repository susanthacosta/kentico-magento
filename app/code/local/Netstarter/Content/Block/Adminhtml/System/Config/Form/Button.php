<?php

class Netstarter_Content_Block_Adminhtml_System_Config_Form_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('netstarter/content/system/config/button.phtml');
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $configData = Mage::getStoreConfig('content/config');
        $kenticoUrl = $configData["kenticourl"] . "cmspages/siglesignon.aspx?username=" . Mage::getSingleton('admin/session')->getUser()->getUsername();
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'id'        => 'sso_button',
            'label'     => $this->helper('adminhtml')->__('Go to Kentico'),
            'onclick'   => 'javascript:window.location.href=\'' . $kenticoUrl . '\';'
        ));

        return $button->toHtml();
    }
}