<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_category', 'kentico_banner_path', array(
    'group'         => 'General Information',
    'input'         => 'text',
    'type'          => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'label'         => 'Kentico Banner Path',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
/*
 * Resets DDL cache since we changed schema.
 */
$installer->getConnection()->resetDdlCache();
$installer->endSetup();